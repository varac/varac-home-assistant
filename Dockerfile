# Todo:
#   * Renovate music-assistant

# https://hub.docker.com/r/homeassistant/home-assistant/tags
# Based on Alpine Linux v3.18
# 2023.8 breaks the build, see https://0xacab.org/varac-projects/varac-home-assistant/-/jobs/449322
FROM registry.hub.docker.com/homeassistant/home-assistant:2024.1.3

# renovate: datasource=repology depName=alpine_3_18/gcc
ENV GCC_VERSION="12.2.1_git20220924-r10"
# renovate: datasource=repology depName=alpine_3_18/musl-dev
ENV MUSL_DEV_VERSION="1.2.4-r2"
# renovate: datasource=repology depName=alpine_3_18/zlib-dev
ENV ZLIB_DEV_VERSION="1.2.13-r1"
# renovate: datasource=repology depName=alpine_3_18/jpeg-dev
ENV JPEG_DEV_VERSION="9e-r1"
# renovate: datasource=repology depName=alpine_3_18/python3-dev
ENV PYTHON3_DEV_VERSION="3.11.6-r0"
# renovate: datasource=repology depName=alpine_3_18/poetry
ENV POETRY_VERSION="1.4.2-r1"
# https://github.com/music-assistant/hass-music-assistant/releases/
#
# From https://github.com/music-assistant/hass-music-assistant#attention-running-home-assistant-20233-or-later:
#   Attention: Running Home Assistant 2023.3 or later?
#   Make sure to install the BETA version of Music Assistant (2023.6.bx). Older versions
#   of Music Assistant are not compatible with recent Home Assistant versions. We expect
#   to end our beta stage soon.

ENV MUSIC_ASSISTANT_VERSION="2023.12.0"

WORKDIR /tmp
ADD https://github.com/music-assistant/hass-music-assistant/releases/download/${MUSIC_ASSISTANT_VERSION}/mass.zip /tmp
COPY poetry.lock pyproject.toml ./
RUN mkdir -p /tmp/custom_components/mass \
  && unzip /tmp/mass.zip -d /tmp/custom_components/mass \
  && ls -al /tmp/custom_components/mass \
  && rm /usr/local/bin/python \
  && apk add --no-cache --virtual .build-deps \
  gcc=$GCC_VERSION \
  g++=$GCC_VERSION \
  musl-dev=$MUSL_DEV_VERSION \
  zlib-dev=$ZLIB_DEV_VERSION \
  jpeg-dev=$JPEG_DEV_VERSION \
  python3-dev=${PYTHON3_DEV_VERSION} \
  poetry=${POETRY_VERSION}
# https://stackoverflow.com/a/74784239
RUN poetry config virtualenvs.create false \
  && poetry config installer.max-workers 10 \
  && poetry install --no-interaction --no-ansi \
  && apk del .build-deps gcc g++ musl-dev zlib-dev jpeg-dev python3-dev poetry
